#
# Be sure to run `pod lib lint iOSSwiftThaiAddress.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'iOSSwiftThaiAddress'
  s.version          = '3.0.16'
  s.summary          = 'Add searching with english'
  s.swift_version    = '4.2'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://ExcelentePop@bitbucket.org/ExcelentePop/iosswiftthaiaddress/src'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'nattaponpop' => 'excelentepop@gmail.com' }
  s.source           = { :git => 'https://ExcelentePop@bitbucket.org/ExcelentePop/iosswiftthaiaddress.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '9.3'
  s.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
  s.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }

  s.source_files = 'iOSSwiftThaiAddress/Classes/**/*'
  
  s.resource_bundles = {
    'iOSSwiftThaiAddress' => ['iOSSwiftThaiAddress/Assets/*.{json,plist}']
  }
  
  s.static_framework = true
  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
  
  s.dependency 'Firebase/Core', '6.15.0'
  s.dependency 'Firebase/Storage'
#  s.dependency 'Zip', '1.1'
  
end
