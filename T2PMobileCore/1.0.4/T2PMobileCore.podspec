#
# Be sure to run `pod lib lint T2PMobileCore.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'T2PMobileCore'
  s.version          = '1.0.4'
  s.summary          = 'A short description of T2PMobileCore.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://bitbucket.org/ExcelentePop/t2pmobilecore/src/master/'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Nattapon' => 'excelentepop@gmail.com' }
  s.source           = { :git => 'https://ExcelentePop@bitbucket.org/ExcelentePop/t2pmobilecore.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'
  
  s.ios.deployment_target = '9.3'
  
  s.source_files = 'T2PMobileCore/Classes/**/*.{swift}'
  
  s.resource_bundles = {
      'T2PMobileCore' => ['T2PMobileCore/Classes/**/*.{storyboard,xib,xcassets,json,imageset,png}']
  }
  
  s.dependency 'FontAwesome.swift'
  s.dependency 'PNSUtils'
  
end
